﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;
using Xamvvm;

namespace setter
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            MainPage = new setter.MainPage();

            var factory = new XamvvmFormsFactory(this);
            //factory.RegisterNavigationPage<StartNavigation_VM, StartPage_VM>();
            factory.RegisterNavigationPage<MainNavigationPage_VM>(() => this.GetPageFromCache<MainPage_VM>());
            XamvvmCore.SetCurrentFactory(factory);


            Current.MainPage = this.GetPageFromCache<MainNavigationPage_VM>() as NavigationPage;

        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
