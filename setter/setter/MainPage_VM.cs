﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using Xamvvm;

namespace setter
{
    public partial class MainPage_VM : BasePageModel
    {
        public MainPage_VM()
        {
            EatMeButtonPressed = new Command(() => EatMeButtonPressed_Action());
            DrinkMeButtonPressed = new Command(() => DrinkMeButtonPressed_Action());
        }
        int setter_called_count = 0;
        private string _SetterCalled = "0";
        public string SetterCalled
        {
            get
            {
                return _SetterCalled;
            }
        }
        private void DrinkMeButtonPressed_Action()
        {
            _TheEntryText = "DRINK ME!";
            OnPropertyChanged("TheEntryText");
        }

        private void EatMeButtonPressed_Action()
        {
            _TheEntryText = "EAT ME!";
            OnPropertyChanged("TheEntryText");
        }

        private string _TheEntryText = null;
        public string TheEntryText
        {
            get
            {
                return _TheEntryText;
            }
            set
            {
                setter_called_count += 1;
                _SetterCalled = string.Format("setter called {0} times", setter_called_count);
                OnPropertyChanged("SetterCalled");
                _TheEntryText = value;
                //OnPropertyChanged("TheEntryText");
            }
        }

        public ICommand EatMeButtonPressed { get; private set; }
        public ICommand DrinkMeButtonPressed { get; private set; }
    }
}
